# Projeto crud-sepla-api
Desenvolvida em **Spring boot**, a api tem como objetivo **cadastra, editar, listar, remover Cliente e fazer upload e download** de documento.



## Importante
Ao baixar o projeto, os seguintes métodos da class **ParametroService** devem ser alterados, para o caminho de sua escolha.

```java
@Service
public class ParametroService {
	
	@Autowired
	private ParametroRepository parametroRepository;

	public void insert() {
		if(!this.parametroRepository.findByCaminhoLike("**/Users/esmayktillesse/Documents/crud-seplag**")
			.isPresent()) {
			this.parametroRepository.save(new Parametro(null, "/Users/esmayktillesse/Documents/crud-seplag"));
		}
			
	}


	public Parametro getCaminho() throws Exception {
		return this.parametroRepository
				.findByCaminhoLike("**/Users/esmayktillesse/Documents/crud-seplag**")
				.orElseThrow(() -> new Exception("caminho nao localizado."));
	}

}
