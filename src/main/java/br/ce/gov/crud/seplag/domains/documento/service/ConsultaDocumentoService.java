package br.ce.gov.crud.seplag.domains.documento.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.documento.model.Documento;
import br.ce.gov.crud.seplag.domains.documento.repository.DocumentoRepository;
import br.ce.gov.crud.seplag.exceptions.ObjectNotFoundException;

@Service
public class ConsultaDocumentoService {
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	public Documento findById(Integer id) {
		Optional<Documento> obj = this.documentoRepository.findById(id);
		return obj.orElseThrow(() -> 
			new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Documento.class.getName()));
	}
}
