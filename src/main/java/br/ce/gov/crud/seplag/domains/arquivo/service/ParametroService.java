package br.ce.gov.crud.seplag.domains.arquivo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.arquivo.model.Parametro;
import br.ce.gov.crud.seplag.domains.arquivo.repository.ParametroRepository;

@Service
public class ParametroService {
	
	@Autowired
	private ParametroRepository parametroRepository;
	
	public void insert() {
		if(!this.parametroRepository.findByCaminhoLike("/Users/esmayktillesse/Documents/crud-seplag").isPresent()) {
			this.parametroRepository.save(new Parametro(null, "/Users/esmayktillesse/Documents/crud-seplag"));
		}
			
	}
	
	
	public Parametro getCaminho() throws Exception {
		return this.parametroRepository
				.findByCaminhoLike("/Users/esmayktillesse/Documents/crud-seplag")
				.orElseThrow(() -> new Exception("caminho nao localizado."));
	}

}
