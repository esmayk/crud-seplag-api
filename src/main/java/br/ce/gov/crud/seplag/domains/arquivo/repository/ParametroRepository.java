package br.ce.gov.crud.seplag.domains.arquivo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ce.gov.crud.seplag.domains.arquivo.model.Parametro;

@Repository
public interface ParametroRepository extends JpaRepository<Parametro, Integer> {
	
	Optional<Parametro> findByCaminhoLike(String caminho);

}
