package br.ce.gov.crud.seplag.domains.cliente.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.cliente.dto.ClienteUpdateDTO;
import br.ce.gov.crud.seplag.domains.cliente.model.Cliente;
import br.ce.gov.crud.seplag.domains.cliente.repository.ClienteRepository;

@Service
public class AlteraClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private ConsultaClienteService consultaClienteService;
	
	public Cliente update(ClienteUpdateDTO objDto) {
		Cliente newObj = this.consultaClienteService.findById(objDto.getId());
		updateData(newObj, objDto);
		return this.clienteRepository.save(newObj);
	}
	
	private void updateData(Cliente newObj, ClienteUpdateDTO objDto) {
		newObj.setNome(objDto.getNome());
		newObj.setEmail(objDto.getEmail());
		newObj.setCpf(objDto.getCpf());
		newObj.setCnpj(objDto.getCnpj());
	}
	
}
