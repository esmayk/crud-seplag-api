package br.ce.gov.crud.seplag.domains.cliente.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.ce.gov.crud.seplag.domains.cliente.dto.ClienteDTO;
import br.ce.gov.crud.seplag.domains.cliente.dto.ClienteNewDTO;
import br.ce.gov.crud.seplag.domains.cliente.dto.ClienteUpdateDTO;
import br.ce.gov.crud.seplag.domains.cliente.model.Cliente;
import br.ce.gov.crud.seplag.domains.cliente.service.AlteraClienteService;
import br.ce.gov.crud.seplag.domains.cliente.service.ConsultaClienteService;
import br.ce.gov.crud.seplag.domains.cliente.service.RemoveClienteService;
import br.ce.gov.crud.seplag.domains.cliente.service.SalvaClienteService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value="/clientes")
public class ClienteController {
	
	@Autowired
	private SalvaClienteService salvaClienteService;
	
	@Autowired 
	private ConsultaClienteService consultaClienteService;
	
	@Autowired
	private RemoveClienteService removeClienteService;
	
	@Autowired
	private AlteraClienteService alteraClienteService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> find(@PathVariable Integer id) {
		Cliente obj = this.consultaClienteService.findById(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ClienteDTO>> findAll() {
		List<Cliente> list = this.consultaClienteService.findAll();
		List<ClienteDTO> listDto = list.stream().map(obj -> new ClienteDTO(obj)).collect(Collectors.toList());  
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Cliente> insert(@Valid @RequestBody ClienteNewDTO objDto) {
		Cliente obj = this.salvaClienteService.fromDTO(objDto);
		obj = this.salvaClienteService.insert(obj);
		return ResponseEntity.ok(obj);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Cliente> update(@Valid @RequestBody ClienteUpdateDTO objDto, @PathVariable Integer id) {
		this.alteraClienteService.update(objDto);
		return ResponseEntity.noContent().build();
	}
	
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		this.removeClienteService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
