package br.ce.gov.crud.seplag.domains.cliente.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.cliente.repository.ClienteRepository;

@Service
public class RemoveClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired 
	private ConsultaClienteService consultaClienteService;

	public void delete(Integer id) {
		this.consultaClienteService.findById(id);
		this.clienteRepository.deleteById(id);
	}
	
}
