package br.ce.gov.crud.seplag.domains.cliente.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.ce.gov.crud.seplag.domains.cliente.enums.TipoCliente;
import br.ce.gov.crud.seplag.domains.documento.model.Documento;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@EqualsAndHashCode
@ToString(exclude = "id")
@NoArgsConstructor
@AllArgsConstructor
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Integer id;

	@Getter
	@Setter
	private String nome;

	@Column(unique = true)
	@Getter
	@Setter
	private String email;

	@Getter
	@Setter
	private String cpf;

	@Getter
	@Setter
	private String cnpj;

	private Integer tipo;

	@Getter
	@Setter
	private LocalDateTime dataCadastro;

	@OneToMany(mappedBy = "cliente")
	@Getter
	@Setter
	private List<Documento> documentos = new ArrayList<>();

	public TipoCliente getTipo() {
		return TipoCliente.toEnum(tipo);
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo.getCod();
	}

}
