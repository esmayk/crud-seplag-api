package br.ce.gov.crud.seplag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudSeplagApiApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(CrudSeplagApiApplication.class, args);
		
	}

}
