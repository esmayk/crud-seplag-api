package br.ce.gov.crud.seplag.domains.documento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.ce.gov.crud.seplag.domains.arquivo.service.DownloadArquivoService;
import br.ce.gov.crud.seplag.domains.arquivo.service.UploadArquivoService;

@RestController
@RequestMapping(value="/documentos")
public class DocumentoController {
	
	@Autowired
	private UploadArquivoService uploadArquivoService;
	
	@Autowired
	private DownloadArquivoService downloadArquivoService;
	
	
	@RequestMapping(value = "/upload/{idCliente}", method = RequestMethod.POST)
	public ResponseEntity<Void> upload(@RequestParam(value = "arquivo")  MultipartFile arquivo,
			@PathVariable("idCliente") Integer idCliente) throws Exception {
		this.uploadArquivoService.upload(arquivo, idCliente);
		return ResponseEntity.ok().build();
	}
	
	@RequestMapping(value="/download/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> download(@PathVariable("id") Integer id)
			throws Exception {		
		return this.downloadArquivoService.download(id);
	}

}
