package br.ce.gov.crud.seplag.domains.arquivo.model;

import java.io.InputStream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor @NoArgsConstructor
public class Arquivo {

	@Getter @Setter
	private String nome;
	@Getter @Setter
	private InputStream conteudo;
	
	public String nomeSemExtensao() {
		return getNome().substring(0, getNome().lastIndexOf("."));
	}
	
	public String getExtensao() {
		return getNome().substring(getNome().lastIndexOf("."), getNome().length());
	}
}
