package br.ce.gov.crud.seplag.domains.cliente.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import br.ce.gov.crud.seplag.domains.cliente.validation.ClienteInsert;
import lombok.Getter;
import lombok.Setter;

@ClienteInsert
public class ClienteNewDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	private Integer id;
	
	@Getter @Setter
	@NotEmpty(message="Preenchimento obrigatório")
	@Length(min=5, max=120, message="O tamanho deve ser entre 5 e 120 caracteres")
	private String nome;
	
	@Column(unique = true)
	@Getter @Setter 
	@Email(message="Email inválido")
	private String email;
	
	@Getter @Setter @CPF
	private String cpf;
	
	@Getter @Setter @CNPJ
	private String cnpj;
		
	@Getter @Setter
	private LocalDateTime dataCadastro;

}
