package br.ce.gov.crud.seplag.domains.cliente.dto;

import lombok.Getter;
import lombok.Setter;

public class ClienteUpdateDTO {

	@Getter @Setter
	private Integer id;
	
	@Getter @Setter
	private String nome;
	
	@Getter @Setter 
	private String email;
	
	@Getter @Setter 
	private String cpf;
	
	@Getter @Setter 
	private String cnpj;
	
}
