package br.ce.gov.crud.seplag.domains.arquivo.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.documento.model.Documento;
import br.ce.gov.crud.seplag.domains.documento.service.ConsultaDocumentoService;

@Service
public class DownloadArquivoService {
	
	@Autowired
	private ConsultaDocumentoService consultaDocumentoService;
	
	public ResponseEntity<InputStreamResource> download(Integer id) throws Exception {	
		Documento documento = this.consultaDocumentoService.findById(id);
		File file = new File(documento.getCaminho() + File.separator + documento.getNome());
		InputStream pdfStream = new FileInputStream(file);
		return this.pdf(file.getName(), pdfStream);
	}

	
	private ResponseEntity<InputStreamResource> pdf(String tituloArquivo, InputStream pdfStream) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("inline; filename=%s", tituloArquivo));

        return ResponseEntity
          .ok()
          .headers(headers)
          .contentType(MediaType.APPLICATION_PDF)
          .body(new InputStreamResource(pdfStream));

    }
}
