package br.ce.gov.crud.seplag.domains.documento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ce.gov.crud.seplag.domains.documento.model.Documento;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Integer> {

}
