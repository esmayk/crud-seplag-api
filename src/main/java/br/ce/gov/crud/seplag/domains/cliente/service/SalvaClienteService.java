package br.ce.gov.crud.seplag.domains.cliente.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ce.gov.crud.seplag.domains.cliente.dto.ClienteNewDTO;
import br.ce.gov.crud.seplag.domains.cliente.enums.TipoCliente;
import br.ce.gov.crud.seplag.domains.cliente.model.Cliente;
import br.ce.gov.crud.seplag.domains.cliente.repository.ClienteRepository;

@Service
public class SalvaClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Transactional
	public Cliente insert(Cliente obj) {
		obj.setId(null);
		obj = this.clienteRepository.save(obj);
		return obj;
	}

	public Cliente fromDTO(ClienteNewDTO objDto) {
		Cliente cli = new Cliente();
		cli.setId(null);
		cli.setNome(objDto.getNome());
		cli.setEmail(objDto.getEmail());
		
		if (objDto.getCpf() != null && !objDto.getCpf().equals("")) {
			cli.setCpf(objDto.getCpf());
			cli.setTipo(TipoCliente.PESSOAFISICA);
		} else if (objDto.getCnpj() != null && !objDto.getCnpj().equals("")) {
			cli.setCnpj(objDto.getCnpj());
			cli.setTipo(TipoCliente.PESSOAJURIDICA);
		}
		cli.setDataCadastro(LocalDateTime.now());
		return cli;
	}

}
