package br.ce.gov.crud.seplag.domains.cliente.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.cliente.model.Cliente;
import br.ce.gov.crud.seplag.domains.cliente.repository.ClienteRepository;
import br.ce.gov.crud.seplag.exceptions.ObjectNotFoundException;

@Service
public class ConsultaClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public List<Cliente> findAll() {
		return this.clienteRepository.findAll();
	}
	
	
	public Cliente findById(Integer id) {
		Optional<Cliente> obj = this.clienteRepository.findById(id);
		return obj.orElseThrow(() -> 
			new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName()));
	}
}
