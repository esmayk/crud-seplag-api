package br.ce.gov.crud.seplag.domains.documento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.documento.model.Documento;
import br.ce.gov.crud.seplag.domains.documento.repository.DocumentoRepository;

@Service
public class SalvaDocumentoService {

	@Autowired
	private DocumentoRepository documentoRepository;
	
	public void insert(Documento documento) {
		this.documentoRepository.save(documento);
	}
}
