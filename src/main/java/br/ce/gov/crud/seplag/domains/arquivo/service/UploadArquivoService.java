package br.ce.gov.crud.seplag.domains.arquivo.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.ce.gov.crud.seplag.domains.cliente.service.ConsultaClienteService;
import br.ce.gov.crud.seplag.domains.documento.model.Documento;
import br.ce.gov.crud.seplag.domains.documento.service.SalvaDocumentoService;

@Service
public class UploadArquivoService {

	private static final String NAO_ASCII = "[^\\p{ASCII}]";
	
	@Autowired
	private ArquivoService arquivoService;
	
	@Autowired
	private ParametroService parametroService;
	
	@Autowired
	private SalvaDocumentoService salvaDocumentoService;
	
	@Autowired
	private ConsultaClienteService consultaClienteService;

	public void upload(MultipartFile arquivo, Integer idCliente) throws Exception {
		this.geraArquivo(arquivo, idCliente);
	}

	private String geraArquivo(MultipartFile file, Integer idCliente) throws Exception {
		String nomeArquivo = file.getOriginalFilename();
		InputStream is;
		try {
			is = file.getInputStream();
		} catch (IOException e) {
			throw new Exception("Não foi possivel ler o arquivo informado");
		}
		return this.salvaArquivo(nomeArquivo, is, idCliente);
	}

	private String salvaArquivo(String nomeOriginal, InputStream is, Integer idCliente) throws Exception {

		nomeOriginal = removerCaracteresEspeciais(nomeOriginal);

		String extension = FilenameUtils.getExtension(nomeOriginal);
		if (extension.isEmpty()) {
			throw new Exception("Extensão do arquivo não informada");
		}

		String novoNome = UUID.randomUUID().toString() + "_" + nomeOriginal;
		
		// verifica se o parametro exite, caso nao ele grava.
		this.parametroService.insert();
		String rootPath = this.parametroService.getCaminho().getCaminho();

		String filePath = rootPath + File.separator + novoNome;

		try {
			//byte[] bytes = IOUtils.toByteArray(is);
			if (this.arquivoService.salvarArquivo(filePath, is)) {
				
				Documento documento = new Documento();
				documento.setNome(novoNome);
				documento.setCaminho(rootPath);
				documento.setData(LocalDateTime.now());
				documento.setCliente(this.consultaClienteService.findById(idCliente));
				//documento.setArquivo(bytes);
				
				this.salvaDocumentoService.insert(documento);
				
				return filePath;
			} else {
				throw new Exception("Não foi possível salvar o arquivo");
			}
		} catch (IOException e) {
			throw new Exception("Não foi possível salvar o arquivo", e);
		}
	}

	private String removerCaracteresEspeciais(String string) {
		if (string == null) {
			return "";
		}
		string = Normalizer.normalize(string, Normalizer.Form.NFD);
		return string.replaceAll(NAO_ASCII, "");
	}

}
