package br.ce.gov.crud.seplag.domains.arquivo.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ce.gov.crud.seplag.domains.arquivo.model.Arquivo;

@Service
public class ArquivoService {
	
	@Autowired
	private ParametroService parametroService;
	
	
	public String montarCaminhoCompleto(String caminhoRelativo) throws Exception {
		this.parametroService.insert();
		String rootPath = this.parametroService.getCaminho().getCaminho();
		return rootPath + caminhoRelativo;
	}
	
	public boolean salvarArquivo(String uri, InputStream arquivoIS) throws IOException {
		File arquivo = null;
		OutputStream outputStream = null;
		try  {
			arquivo = new File(uri);
			if (arquivo.exists()) {
				arquivo.delete();
			}
			criarDiretorios(arquivo);
			arquivo.createNewFile();
			outputStream = new FileOutputStream(arquivo);
			IOUtils.copy(arquivoIS, outputStream);
			
			return true;
		} catch (IOException ex) {
			try {
				if (arquivo != null && arquivo.exists()) {
					arquivo.delete();
				}
			} catch (Exception exInterno) {
				return false;
			}
			throw ex;
		} finally {
			IOUtils.closeQuietly(arquivoIS);
			IOUtils.closeQuietly(outputStream);
		}
	}
	
	public boolean isExisteArquivo(String url) {
		File arquivo = new File(url);
		
		return arquivo.exists();
	}

	public Arquivo getArquivo(String url) throws IOException, IllegalStateException {
		
		File arquivo = new File(url);
		
		if (!arquivo.exists()) {
			throw new IllegalStateException("Arquivo não encontrado: " + url);
			
		}
		return new Arquivo(arquivo.getName(), new FileInputStream(arquivo));
	}
	
	private void criarDiretorios(File arquivo) {
		try {
			String caminhoDiretorio = arquivo.getPath().substring(0, arquivo.getPath().lastIndexOf(File.separator));
			File caminho = new File(caminhoDiretorio);
						
			if (!caminho.exists()) {
				caminho.mkdirs();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
