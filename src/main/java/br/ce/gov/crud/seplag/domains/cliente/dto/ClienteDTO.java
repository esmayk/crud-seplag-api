package br.ce.gov.crud.seplag.domains.cliente.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import br.ce.gov.crud.seplag.domains.cliente.enums.TipoCliente;
import br.ce.gov.crud.seplag.domains.cliente.model.Cliente;
import br.ce.gov.crud.seplag.domains.documento.model.Documento;
import lombok.Getter;
import lombok.Setter;

public class ClienteDTO {

	@Getter @Setter
	private Integer id;
	
	@Getter @Setter
	private String nome;
	
	@Getter @Setter 
	private String email;
	
	@Getter @Setter 
	private String cpf;
	
	@Getter @Setter 
	private String cnpj;
	
	private Integer tipo;
	
	@Getter @Setter
	private LocalDateTime dataCadastro;
	
	@Getter @Setter
	private List<Documento> documentos = new ArrayList<>();
	
	public TipoCliente getTipo() {
		return TipoCliente.toEnum(tipo);
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo.getCod();
	}
	
	public ClienteDTO(Cliente obj) {
		id = obj.getId();
		nome = obj.getNome();
		email = obj.getEmail();
		cpf = obj.getCpf();
		cnpj = obj.getCnpj();
		tipo = obj.getTipo().getCod();
		dataCadastro = obj.getDataCadastro();
		documentos.addAll(obj.getDocumentos());
	}
	
}
